﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BootcampCoreServices;

namespace UnitTestCoreServices
{
    [TestClass]
    public class UtilsTest
    {




        //MethodName_Scenario_ExpectedBehavior()
        [TestMethod]
        public void parseERRORSObjectNEW_AllFieldsAreOk_returnFalse()
        {
            //Arrange
            string ErrorsList = "";
            int reuqestNumbers = 1;
            string clientId = "ASDQWE";
            Request reuqest = new Request(clientId, 1, "NONE", 1, 1);

            //Act
            bool result =  Utils.parseERRORSObjectNEW(reuqest, ref ErrorsList, ref reuqestNumbers);

            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void parseERRORSObjectNEW_ClienttIdIncludeSpace_returnTrue()
        {
            //Arrange
            string ErrorsList = "";
            int reuqestNumbers = 1;
            string clientId = "ASD WE";
            Request reuqest = new Request(clientId, 1, "NONE", 1, 1);

            //Act
            bool result = Utils.parseERRORSObjectNEW(reuqest, ref ErrorsList, ref reuqestNumbers);

            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void parseERRORSObjectNEW_ClienttIdLongThenSix_returnTrue()
        {
            //Arrange
            string ErrorsList = "";
            int reuqestNumbers = 1;
            string clientId = "ASDQWEQWE";
            Request reuqest = new Request(clientId, 1, "NONE", 1, 1);

            //Act
            bool result = Utils.parseERRORSObjectNEW(reuqest, ref ErrorsList, ref reuqestNumbers);

            //Assert
            Assert.IsTrue(result);
        }


        [TestMethod]
        public void parseERRORSObjectNEW_ClienttIdIsEmpty_returnTrue()
        {
            //Arrange
            string ErrorsList = "";
            int reuqestNumbers = 1;
            string clientId = "";
            Request reuqest = new Request(clientId, 1, "NONE", 1, 1);

            //Act
            bool result = Utils.parseERRORSObjectNEW(reuqest, ref ErrorsList, ref reuqestNumbers);

            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void parseERRORSObjectNEW_ClienttIdIsNULL_returnTrue()
        {
            //Arrange
            string ErrorsList = "";
            int reuqestNumbers = 1;
            string clientId = null;
            Request reuqest = new Request(clientId, 1, "NONE", 1, 1);

            //Act
            bool result = Utils.parseERRORSObjectNEW(reuqest, ref ErrorsList, ref reuqestNumbers);

            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void parseERRORSObjectNEW_RequestIdIsnull_returnTrue()
        {
            //Arrange
            string ErrorsList = "";
            int reuqestNumbers = 1;
            string clientId = "ASDQWE";
            long? requestId = null;
            Request reuqest = new Request(clientId, requestId, "NONE", 1, 1);

            //Act
            bool result = Utils.parseERRORSObjectNEW(reuqest, ref ErrorsList, ref reuqestNumbers);

            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void parseERRORSObjectNEW_RequestIdisMinusOne_returnTrue()
        {
            //Arrange
            string ErrorsList = "";
            int reuqestNumbers = 1;
            string clientId = "ASDQWE";
            long? requestId = -1;
            Request reuqest = new Request(clientId, requestId, "NONE", 1, 1);

            //Act
            bool result = Utils.parseERRORSObjectNEW(reuqest, ref ErrorsList, ref reuqestNumbers);

            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void parseERRORSObjectNEW_NameisLongerThen255_returnTrue()
        {
            //Arrange
            string ErrorsList = "";
            int reuqestNumbers = 1;
            string name = "NONONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENONENON";
            Request reuqest = new Request("ASDQWE", 1, name, 1, 1);

            //Act
            bool result = Utils.parseERRORSObjectNEW(reuqest, ref ErrorsList, ref reuqestNumbers);

            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void parseERRORSObjectNEW_NameisEmpty_returnTrue()
        {
            //Arrange
            string ErrorsList = "";
            int reuqestNumbers = 1;
            string name = "";
            Request reuqest = new Request("ASDQWE", 1, name, 1, 1);

            //Act
            bool result = Utils.parseERRORSObjectNEW(reuqest, ref ErrorsList, ref reuqestNumbers);

            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void parseERRORSObjectNEW_NameisNULL_returnTrue()
        {
            //Arrange
            string ErrorsList = "";
            int reuqestNumbers = 1;
            string name = null;
            Request reuqest = new Request("ASDQWE", 1, name, 1, 1);

            //Act
            bool result = Utils.parseERRORSObjectNEW(reuqest, ref ErrorsList, ref reuqestNumbers);

            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void parseERRORSObjectNEW_QuantityIsMinusOne_returnTrue()
        {
            //Arrange
            string ErrorsList = "";
            int reuqestNumbers = 1;
            int? quantity = -1;
            Request reuqest = new Request("ASDQWE", 1, "NONE", quantity, 1);

            //Act
            bool result = Utils.parseERRORSObjectNEW(reuqest, ref ErrorsList, ref reuqestNumbers);

            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void parseERRORSObjectNEW_QuantityIsZero_returnTrue()
        {
            //Arrange
            string ErrorsList = "";
            int reuqestNumbers = 1;
            int? quantity = 0;
            Request reuqest = new Request("ASDQWE", 1, "NONE", quantity, 1);

            //Act
            bool result = Utils.parseERRORSObjectNEW(reuqest, ref ErrorsList, ref reuqestNumbers);

            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void parseERRORSObjectNEW_QuantityIsNULL_returnTrue()
        {
            //Arrange
            string ErrorsList = "";
            int reuqestNumbers = 1;
            int? quantity = null;
            Request reuqest = new Request("ASDQWE", 1, "NONE", quantity, 1);

            //Act
            bool result = Utils.parseERRORSObjectNEW(reuqest, ref ErrorsList, ref reuqestNumbers);

            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void parseERRORSObjectNEW_PriceIsNULL_returnTrue()
        {
            //Arrange
            string ErrorsList = "";
            int reuqestNumbers = 1;
            double? price = null;
            Request reuqest = new Request("ASDQWE", 1, "NONE", 1, price);

            //Act
            bool result = Utils.parseERRORSObjectNEW(reuqest, ref ErrorsList, ref reuqestNumbers);

            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void parseERRORSObjectNEW_PriceisMinusOne_returnTrue()
        {
            //Arrange
            string ErrorsList = "";
            int reuqestNumbers = 1;
            double? price = -1;
            Request reuqest = new Request("ASDQWE", 1, "NONE", 1, price);

            //Act
            bool result = Utils.parseERRORSObjectNEW(reuqest, ref ErrorsList, ref reuqestNumbers);

            //Assert
            Assert.IsTrue(result);
        }


        [TestMethod]
        public void parseERRORSObjectNEW_PriceisZero_returnTrue()
        {
            //Arrange
            string ErrorsList = "";
            int reuqestNumbers = 1;
            double? price = 0;
            Request reuqest = new Request("ASDQWE", 1, "NONE", 1, price);

            //Act
            bool result = Utils.parseERRORSObjectNEW(reuqest, ref ErrorsList, ref reuqestNumbers);

            //Assert
            Assert.IsTrue(result);
        }

    }
}
