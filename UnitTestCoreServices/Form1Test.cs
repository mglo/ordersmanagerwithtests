﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BootcampCoreServices;

namespace UnitTestCoreServices
{
    /// <summary>
    /// Summary description for Form1Test
    /// </summary>
    [TestClass]
    public class ReportManagerTests
    {

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void commaTOdot_textIsWithComma_ReturnsTextWithDot()
        {
            //Arrange
            string textIn = "10,22";
            string textExpected = "10.22";
            ReportManager reportmanager = new ReportManager();

            //Act
            string result = reportmanager.commaTOdot(textIn);

            //Assert
            Assert.AreEqual(textExpected, result);
        }

        [TestMethod]
        public void commaTOdot_textIsWithComma_NothingChange()
        {
            //Arrange
            string textIn = "10.22";
            string textExpected = "10.22";
            ReportManager reportmanager = new ReportManager();

            //Act
            string result = reportmanager.commaTOdot(textIn);

            //Assert
            Assert.AreEqual(textExpected, result);
        }
    }
}
