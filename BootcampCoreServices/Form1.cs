﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BootcampCoreServices
{
    public partial class ReportManager : Form
    {
        //Main Storage place for Orders
        SingletonMainListOfRequests mainListOfRequests;

        //Temp table for gathering orders per action
        List<Request> orders;

        public ReportManager()
        {
            InitializeComponent();

            //Initialize Main List of Request 
            mainListOfRequests = SingletonMainListOfRequests.getInstance();

            comboBoxSelectReport.Items.Add("Ilość zamówień");
            comboBoxSelectReport.Items.Add("Ilość zamówień dla klienta o wskazanym identyfikatorze");
            comboBoxSelectReport.Items.Add("Łączna kwota zamówień");
            comboBoxSelectReport.Items.Add("Łączna kwota zamówień dla klienta o wskazanym identyfikatorze");
            comboBoxSelectReport.Items.Add("Lista wszystkich zamówień");
            comboBoxSelectReport.Items.Add("Lista zamówień dla klienta o wskazanym identyfikatorze");
            comboBoxSelectReport.Items.Add("Średnia wartość zamówienia");
            comboBoxSelectReport.Items.Add("Średnia wartość zamówienia dla klienta o wskazanym identyfikatorze");
            comboBoxSelectReport.Items.Add("Ilość zamówień pogrupowanych po nazwie");
            comboBoxSelectReport.Items.Add("Ilość zamówień pogrupowanych po nazwie dla klienta o wskazanym identyfikatorze");
            comboBoxSelectReport.Items.Add("Zamówienia w podanym przedziale cenowym");
            comboBoxSelectReport.SelectedIndex = 0;

           //CreateIfMissing("C:\\temp");
        }

        private void CreateIfMissing(string path)
        {
            bool folderExists = Directory.Exists(path);
            if (!folderExists)
                Directory.CreateDirectory(path);
        }

        private void buttonInsertFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "CSV (*.csv)|*.csv|XML (*.xml)|*.xml|JSON (*.json)|*.json";
            //openFileDialog.InitialDirectory = @"C:\temp\";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string filePath = openFileDialog.FileName;
                string fileExtension = Path.GetExtension(filePath);
                using (StreamReader reader = new StreamReader(filePath))
                {

                    Tuple<List<Request>, string> outputOfFileValidation;

                    //reading data from selected file
                    outputOfFileValidation = Utils.fileReader(reader, fileExtension);

                    //filling orders and errors
                    orders = outputOfFileValidation.Item1;
                    string errors = outputOfFileValidation.Item2;

                    //If any ERRORS were returned
                    if (!String.IsNullOrEmpty(errors))
                    {
                        //if any data were retirned pring to DataGridView
                        if (orders.Count != 0)
                        {
                            DialogResult result = MessageBox.Show("File parser encounter errors, continue and skip invalid lines? \r\n " + errors, "Import Error ", MessageBoxButtons.YesNo);
                            if (result == DialogResult.Yes)
                            {
                                //replicating orders in to list responsible 
                                dataGridView1.DataSource = orders;

                                //refreshing main View with orders
                                addDataToView(orders);
                            }
                        }
                        else
                        {
                            MessageBox.Show(errors, "File can't be parsed");
                        }

                    }
                    else
                    {
                        //refreshing main View with orders
                        addDataToView(orders);
                    }
                }
               
            }

        }

        //Generate Report Button Action Event
        private void buttonGenerateReport_Click(object sender, EventArgs e)
        {
            string selectedReportName = this.comboBoxSelectReport.GetItemText(this.comboBoxSelectReport.SelectedItem);
            if (mainListOfRequests.getList().Count > 0)
            {
                if (selectedReportName.Contains("identyfikatorze"))
                {
                    string clientNumber = textBoxClientNumber.Text;
                    if (!String.IsNullOrEmpty(clientNumber))
                    {
                        if (clientNumber.Length <= 6 && !clientNumber.Contains(' ') && !String.IsNullOrEmpty(clientNumber))
                        {
                            DataTable dataToShow = Utils.provideSingleValueReport(mainListOfRequests.getList(), selectedReportName, textBoxClientNumber.Text);
                            showDataList(dataToShow);
                        }
                        else
                        {
                            //When ClientId is include wrong characters
                            MessageBox.Show("Invalid Client Number. \r\n - max 6 characters (inclusive) \r\n - no spaces inside number \r\n - i.e. ASDQWE", "Client Number Validaiton");
                        }
                    }
                    else
                    {
                        //When ClientId is empty
                        MessageBox.Show("Please provide Client Number. \r\n - max 6 chards (inclusive) \r\n - no spaces inside number \r\n - i.e. ASDQWE", "Client Number Validaiton");
                    }
                }
                else if (selectedReportName.Equals("Lista wszystkich zamówień"))
                {
                    showDataMainList();
                }
                else if (selectedReportName.Equals("Zamówienia w podanym przedziale cenowym"))
                {
                    //when something was provided
                    if (!String.IsNullOrEmpty(textBoxPriceFrom.Text) || !String.IsNullOrEmpty(textBoxPriceTo.Text))
                    {
                        double priceFrom = 0;
                        double priceTo = 0;
                        try
                        {
                            priceFrom = Convert.ToDouble(textBoxPriceFrom.Text);
                            priceTo = Convert.ToDouble(textBoxPriceTo.Text);
                        }
                        catch
                        {
                            MessageBox.Show("Invalid Prce Range");
                        }

                        if (priceFrom < priceTo)
                        {
                            DataTable dataToShow = Utils.provideSingleValueReport(mainListOfRequests.getList(), selectedReportName, "ThisWIllBeIgnored", priceFrom, priceTo);
                            showDataList(dataToShow);
                        }
                        else
                        {
                            MessageBox.Show("Price \"from\" cannot be less then price \"to\"");
                        }

                    }
                    else
                    {
                        MessageBox.Show("Please provide Price from and Price To");
                    }
                }
                else
                {
                    //when simple report is requested
                    DataTable dataToShow = Utils.provideSingleValueReport(mainListOfRequests.getList(), selectedReportName);
                    showDataList(dataToShow);
                }
            }
            else
            {
                MessageBox.Show("No data imported.");
            }
        }

        private void addDataToView(List<Request> orders)
        {
            //replicating orders in to list responsible 
            mainListOfRequests.getList().AddRange(orders);
            showDataMainList();
        }

        private void showDataMainList()
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = mainListOfRequests.getList();
            dataGridView1.DataSource = bs;
            fillSortBox();
        }

        private void showDataList(DataTable dt)
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = dt;
            dataGridView1.DataSource = bs;

            //adding columns in to comboBox
            fillSortBox();
        }

        /// <summary>
        /// Exporting DataGridView in to CSV file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonExportCSV_Click(object sender, EventArgs e)
        {
            //TODO Export to CSV 
            //ADD option to set location and name of file
            //convert DataGridView in to CSV ( xxxxx,xxxx,xxx,xxx ) with header

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "CSV (*.csv)|*.csv";
           // openFileDialog.InitialDirectory = @"C:\temp\";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                string fullCSVPath = saveFileDialog.FileName;
                string fileExtension = Path.GetExtension(fullCSVPath);

                // string pathOUT = "C:\\temp\\";
                //string fileNameOUT = "ExportFile.csv";
                //string fullCSVPath = pathOUT + fileNameOUT;

                StringBuilder sbNewCSV = new StringBuilder();

                var sbfromDataGrid = new StringBuilder();

                var headers = dataGridView1.Columns.Cast<DataGridViewColumn>();
                sbfromDataGrid.AppendLine(string.Join(",", headers.Select(column => column.HeaderText).ToArray()));

                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    var cells = row.Cells.Cast<DataGridViewCell>();

                    sbfromDataGrid.AppendLine(string.Join(",", cells.Select(cell => commaTOdot(cell.Value.ToString())).ToArray()));
                }
                //add stringBuilder in to File
                if (File.Exists(fullCSVPath))
                {
                    //TODO notificaiton about overwrite file
                    File.Delete(fullCSVPath);
                }

                using (StreamWriter writer = new StreamWriter(fullCSVPath))
                {
                    writer.Write(sbfromDataGrid.ToString());
                }
                MessageBox.Show("File " + fullCSVPath + " was saved");
            }
        }

        /// <summary>
        /// Helper method to convert commas to dots in output CSV
        /// </summary>
        /// <param name="textToCheck">string with text</param>
        /// <returns></returns>
        public string commaTOdot(string textToCheck)
        {
            if (textToCheck.Contains(","))
            {
                textToCheck = textToCheck.Replace(",", ".");
            }

            return textToCheck;
        }

        private void fillSortBox()
        {
            //remove any previous items 
            comboBoxSortBy.Items.Clear();

            //take from dataGrid information about columns
            int colNumber = dataGridView1.Columns.Count;

            List<string> columnNames = new List<string>();
            for (int i = 0; i < colNumber; i++)
            {
                columnNames.Add(dataGridView1.Columns[i].Name);
            }

            //fill comboBoxSortBy with parameters ASC / DESC? 
            foreach (string column in columnNames)
            {
                comboBoxSortBy.Items.Add(column);
            }
        }

        private void sortDataGridView(List<Request> ordersList, string sortBy)
        {
            //sort list by linq
            List<Request> sortedList = ordersList.OrderByDescending(x => x.GetType().GetProperty(sortBy).GetValue(x, null)).ToList();
            //show data do DataGrid
            BindingSource bs = new BindingSource();
            bs.DataSource = sortedList;
            dataGridView1.DataSource = bs;

            fillSortBox();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string selectedSortValue = this.comboBoxSortBy.GetItemText(this.comboBoxSortBy.SelectedItem);

            if (string.IsNullOrEmpty(selectedSortValue))
            {
                selectedSortValue = this.comboBoxSortBy.GetItemText(this.comboBoxSortBy.Text);
            }

            if (!string.IsNullOrEmpty(selectedSortValue))
            {

                if (dataGridView1.Rows.Count > 1 && dataGridView1.Columns.Count > 1)
                {
                    //sorting here
                    try
                    {
                        List<Request> listOfRequest = ((IEnumerable)dataGridView1.DataSource).Cast<Request>().ToList();
                        sortDataGridView(listOfRequest, selectedSortValue);
                    }
                    catch(Exception)
                    {
                        MessageBox.Show("This Report is not sortable", "Notificaiton Sort");
                    }

                }
            }
            else
            {
                MessageBox.Show("Wrong condition - provided column is incorrect", "Notificaiton Sort");
            }
        }
    }
}
