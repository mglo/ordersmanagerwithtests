﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BootcampCoreServices
{
    [XmlRoot(ElementName ="requests")]
    public class RootObject
        {
        [XmlElement("request")]
        public List<Request> requests { get; set; }
        }

    [XmlRoot(ElementName ="reuqest")]
    public class Request
    {
        public string _Client_Id;
        public long? _Request_id;
        public string _Name;
        public int? _Quantity;
        public double? _Price;


        public Request()
        {
        }

        public Request(string clientId, long? requestId, string name, int? quantity, double? price)
        {
            _Client_Id = clientId;
            _Request_id = requestId;
            _Name = name;
            _Quantity = quantity;
            _Price = price;
        }

        [XmlElement("clientId")]
        [JsonProperty(PropertyName = "clientId")]
        public string Client_Id
        {
            get { return _Client_Id; }
            set { if (String.IsNullOrEmpty(value))
                {
                    _Client_Id = "";
                }
                else
                {
                    _Client_Id = value;
                }
            }
        }

        [XmlElement("requestId")]
        [JsonProperty(PropertyName = "requestId")]
        public long? Request_id
        {
            get { return _Request_id; }
            set
            {
                if (value.Equals(null))
                {
                    _Request_id = -1;
                }
                else
                {
                    _Request_id = value;
                }
            }
        }

        [XmlElement("name")]
        [JsonProperty(PropertyName = "name")]
        public string Name {
            get { return _Name; }
            set
            {
                if (String.IsNullOrEmpty(value))
                    {
                    _Name = "";
                }
                else
                {
                    _Name = value;
                }
            }
        }

        [XmlElement("quantity")]
        [JsonProperty(PropertyName = "quantity")]
        public int? Quantity
        {
            get { return _Quantity; }
            set
            {
                if (value.Equals(null))
                {
                    _Quantity = -1;
                }
                else
                {
                    _Quantity = value;
                }
            }
        }

        [XmlElement("price")]
        [JsonProperty(PropertyName = "price")]
        public double? Price
        {
            get { return _Price; }
            set
            {
                if (value.Equals(null))
                {
                    _Price = -1;
                }
                else
                {
                    _Price = value;
                }
            }
        }

    }
}
